package com.example.colin.assignemnet2;

/**
 * Created by Colin on 2/3/2016.
 */
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Content {

    @SerializedName("city")
    @Expose
    public String city;
    @SerializedName("temp_f")
    @Expose
    public Integer temp;
    @SerializedName("elevation")
    @Expose
    public Integer elevation;
    @SerializedName("relative_humidty")
    @Expose
    public String relativeHumidty;
    @SerializedName("wind_gust_mph")
    @Expose
    public Double windGust;
    @SerializedName("wind_mph")
    @Expose
    public Double windSpeed;
    @SerializedName("weather")
    @Expose
    public String weather;

}
