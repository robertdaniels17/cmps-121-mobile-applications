
package com.example.shobhit.findrestaurant;

//import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Response {

    @SerializedName("conditions")
    @Expose
    public Conditions conditions;
    @SerializedName("result")
    @Expose
    public String result;

    public Conditions getConditions() {
        return conditions;
    }


    public void setConditions(Conditions conditions) {
        this.conditions = conditions;
    }


    public String getResult() {
        return result;
    }


    public void setResult(String result) {
        this.result = result;
    }

}

