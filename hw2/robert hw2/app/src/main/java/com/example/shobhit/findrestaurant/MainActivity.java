package com.example.shobhit.findrestaurant;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.GET;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
        @Override
        protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.activity_main);
        }


        @Override
protected void onResume(){
        super.onResume();
        }
public void Weather(View v){

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient httpClient = new OkHttpClient.Builder()
        .addInterceptor(logging)
        .build();

        Retrofit retrofit = new Retrofit.Builder()
        .baseUrl("http://luca-teaching.appspot.com/weather/")	//We are using Foursquare API to get data
        .addConverterFactory(GsonConverterFactory.create())	//parse Gson string
        .client(httpClient)	//add logging
        .build();

        weatherService service = retrofit.create(weatherService.class);

        //Example = com.example.shobhit.findrestaurant.Example
        Call<RegistrationResponse> queryResponseCall = service.listWeather();

        //Call retrofit asynchronously
        queryResponseCall.enqueue(new Callback<RegistrationResponse>() {

@Override
public void onResponse(Response<RegistrationResponse> response) {
        TextView txtout1 = (TextView) findViewById(R.id.display1);
        TextView txtout2 = (TextView) findViewById(R.id.display2);
        TextView txtout3 = (TextView) findViewById(R.id.display3);
        TextView txtout4 = (TextView) findViewById(R.id.display4);

        String city = response.body().getResponse().getConditions().getObservationLocation().getCity();
        String state = response.body().getResponse().getConditions().getObservationLocation().getCountry();
        String country = response.body().getResponse().getConditions().getObservationLocation().getState();
        Double tempf = response.body().getResponse().getConditions().getTempF();
        Double tempc = response.body().getResponse().getConditions().getTempC();
        Integer windspeed = response.body().getResponse().getConditions().getWindGustMph();
        String lat = response.body().getResponse().getConditions().getObservationLocation().getLatitude();
        String lon = response.body().getResponse().getConditions().getObservationLocation().getLongitude();
        String wind = "" + windspeed + " mph";


        String weather = response.body().getResponse().getConditions().getWeather();
        txtout1.setText(city + "," + state + " " + country);
        txtout2.setText("Temperature: " + tempf + "/" + tempc);
        txtout3.setText("Wind speed: " + wind);
        txtout4.setText("Coordinates: " + lat + "/" + lon);
        }

@Override
public void onFailure(Throwable t) {
        // Log error here since request failed
        }

//parse the response for list of restaurants
private List<com.example.shobhit.findrestaurant.ListElement> getConditions(Response response){
        List<com.example.shobhit.findrestaurant.ListElement> venues;
        venues = new ArrayList<com.example.shobhit.findrestaurant.ListElement>();
        return venues;
        }
        });
        }

/**
 * Foursquare api https://developer.foursquare.com/docs/venues/search
 */
public interface weatherService {
    @GET("default/get_weather")
    Call<RegistrationResponse> listWeather();
}
}
