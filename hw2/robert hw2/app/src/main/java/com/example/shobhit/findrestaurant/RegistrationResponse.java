package com.example.shobhit.findrestaurant;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class RegistrationResponse {

    @SerializedName("response")
    @Expose
    private Response response;


    public Response getResponse() {
        return response;
    }


    public void setResponse(Response response) {
        this.response = response;
    }


}
