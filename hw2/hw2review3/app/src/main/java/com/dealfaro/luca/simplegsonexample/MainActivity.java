package com.dealfaro.luca.simplegsonexample;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.net.*;
import java.io.*;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Query;
import com.google.gson.Gson;

import java.util.prefs.Preferences;

public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG = "json_example";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void clickButton(View v) {
        // Creates some fake data.
       // Retrofit retrofit = new Retrofit.Builder()
         //       .baseUrl("http://luca-teaching.appspot.com/weather/default/get_weather")	//We are using Foursquare API to get data
           //     .addConverterFactory(GsonConverterFactory.create())	//parse Gson string
             //   .build();

      // Content c = new Content();
       // c.name = "Luca";
       // c.score = 32;
        //c.badges.add("Beginner");
        //c.badges.add("Learning");
        // Turns it into a string.
        //Gson gson = new Gson();
        //String s = gson.toJson(c);
        // Let's log it.
        //Log.i(LOG_TAG, "Json string: " + result);
        // And let's write it to the settings.
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //SharedPreferences.Editor e = prefs.edit();
        //e.putString("myprefs", content);
        //e.commit();
        Intent intent = new Intent(this, SecondActivity.class);
        startActivity(intent);



    }




}
