package cs121.homework2.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.lang.String;

/**
 * Created by luca on 28/1/2016.
 */
public class weatherResponse {

    @SerializedName("response")
    @Expose
    public String response;
    @SerializedName("meta")
    @Expose
    public String meta;

}



