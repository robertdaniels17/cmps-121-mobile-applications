package cs121.homework2;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.service.media.MediaBrowserService;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import cs121.homework2.response.*;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG = "HW2";
    private String city;
    private String tempF;
    private String relativeHumidity;
    private String windMph;
    private String windGustMph;
    private String weather;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void onClick(View v){
        Button weatherButton = (Button) findViewById(R.id.weatherButton);
        ((Button) v).setText("Refresh");
        //gets settings, creates user id if missing
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        city = settings.getString("city", null);
        if (city == null){
            //create random one and set it
            SecureRandomString srs = new SecureRandomString();
            city = srs.nextString();
            SharedPreferences.Editor e = settings.edit();
            e.putString("city", city);
            e.commit();
        }

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        //set logging level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://luca-teaching.appspot.com/")
                .addConverterFactory(GsonConverterFactory.create())    //parse Gson string
                .client(httpClient)    //add logging
                .build();

        weatherService service = retrofit.create(weatherService.class);
        Call<weatherResponse>weatherResponseCall = service.getWeather(city, tempF, relativeHumidity, windMph, windGustMph, weather);


        //Call retrofit asynchronously
        weatherResponseCall.enqueue(new Callback<weatherResponse>() {
            @Override
            public void onResponse(Response<weatherResponse> response) {
                TextView cityText = (TextView) findViewById(R.id.cityText);
                TextView tempText = (TextView) findViewById(R.id.tempText);
                TextView rhText = (TextView) findViewById(R.id.rhText);
                TextView windText = (TextView) findViewById(R.id.windText);
                TextView gustText = (TextView) findViewById(R.id.gustText);
                TextView weatherText = (TextView) findViewById(R.id.weatherText);


                Log.i(LOG_TAG, "Code is: " + response.code());
                Log.i(LOG_TAG, "The result is: " + response.body().response);
                cityText.setText("City: " + response.body().response.observationLocation.city);
                tempText.setText("Temp: " + response.body().response.Conditions.tempF);
                rhText.setText("Relative Humidity: " + response.body().response.Conditions,relativeHumidity);
                windText.setText("WindMph: " + response.body().response.Conditions.windMph);
                gustText.setText("WindGustMph: " + response.body().response.Conditions.windGustMph);
                weatherText.setText("Weather: " + response.body().response.Conditions.weather);
            }

            @Override
            public void onFailure (Throwable t){
                // Log error here since request failed
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    public void onPause(){
        super.onPause();
    }

    public interface weatherService {
        @GET("weather/default/get_weather/")
        Call<weatherResponse> getWeather(@Query("city") String city,    //city name
                                         @Query("temp_f") String tempF,  //Temp in fahrenheit
                                         @Query("relative_humidity") String relativeHumidity, //humidity
                                         @Query("wind_mph") String windMph,    //average wind
                                         @Query("wind_gust_mph") String windGustMph,  //gusts
                                         @Query("weather") String weather); //weather

    }
}

