package com.example.danielstansberry.weather_app.Data.remote;

import com.example.danielstansberry.weather_app.Data.model.Weather;

import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.Call;
/**
 * Created by Daniel Stansberry on 2/2/2016.
 */
public interface WeatherAPI {

    String URL = "http://luca-teaching.appspot.com/weather/default/";
    @GET("get_weather") Call<Weather> getWeather();


    class Factory {
        private static WeatherAPI service;
        public static WeatherAPI getInstance() {
            if (service == null) {
                Retrofit retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl(URL).build();
                service = retrofit.create(WeatherAPI.class);
                return service;
            } else {
                return service;
            }
        }
    }
}
