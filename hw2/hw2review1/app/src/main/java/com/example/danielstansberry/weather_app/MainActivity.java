package com.example.danielstansberry.weather_app;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.danielstansberry.weather_app.Data.model.Weather;
import com.example.danielstansberry.weather_app.Data.remote.WeatherAPI;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.w3c.dom.Text;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Query;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.City_ID) TextView textView_city;
    @Bind(R.id.temp_ID) TextView textView_temp;
    @Bind(R.id.elevation_ID) TextView textView_elevation;
    @Bind(R.id.weather_ID) TextView textView_weather;
    @Bind(R.id.windGust_ID) TextView textView_windGust;
    @Bind(R.id.windMPH_ID) TextView textView_windMPH;
    @Bind(R.id.Humidity_ID) TextView textView_humidity;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @OnClick(R.id.refresh_button)
    public void onClick_Button() {
        WeatherAPI.Factory.getInstance().getWeather().enqueue(new Callback<Weather>() {
            @Override
            public void onResponse(Response<Weather> response) {
                if (response.code() == 500)
                {
                    Toast.makeText(MainActivity.this, "500 Error", Toast.LENGTH_SHORT).show();
                }
                else if (response.body().getResponse().getResult().equals("ok"))
                {
                    Toast.makeText(MainActivity.this, "Working...", Toast.LENGTH_SHORT).show();
                    String temp = Double.toString(response.body().getResponse().getConditions().getTempF());
                    textView_temp.setText(temp + " F");
                    String Location = response.body().getResponse().getConditions().getObservationLocation().getCity();
                    textView_city.setText(Location);
                    String elevation = response.body().getResponse().getConditions().getObservationLocation().getElevation();
                    textView_elevation.setText(elevation);
                    String weather = response.body().getResponse().getConditions().getWeather();
                    textView_weather.setText(weather);
                    String windGust = String.valueOf(response.body().getResponse().getConditions().getWindGustMph());
                    textView_windGust.setText("Wind Gust: " + windGust + "mph");
                    String windMPH = String.valueOf(response.body().getResponse().getConditions().getWindMph());
                    textView_windMPH.setText("Wind Speed: " + windMPH + "mph");
                    String humidity = response.body().getResponse().getConditions().getRelativeHumidity();
                    textView_humidity.setText("Humidity: " + humidity);
                }
                else
                     Toast.makeText(MainActivity.this, "Application error", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("Failed", t.getMessage());
            }
        });


    }


    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.danielstansberry.weather_app/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.danielstansberry.weather_app/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
}
