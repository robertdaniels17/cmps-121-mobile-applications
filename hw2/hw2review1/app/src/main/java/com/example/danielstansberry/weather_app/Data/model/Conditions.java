
package com.example.danielstansberry.weather_app.Data.model;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Conditions {

    @SerializedName("wind_gust_mph")
    @Expose
    private int windGustMph;
    @SerializedName("temp_f")
    @Expose
    private double tempF;
    @SerializedName("observation_location")
    @Expose
    private ObservationLocation observationLocation;
    @SerializedName("temp_c")
    @Expose
    private double tempC;
    @SerializedName("relative_humidity")
    @Expose
    private String relativeHumidity;
    @SerializedName("weather")
    @Expose
    private String weather;
    @SerializedName("dewpoint_c")
    @Expose
    private int dewpointC;
    @SerializedName("windchill_c")
    @Expose
    private String windchillC;
    @SerializedName("pressure_mb")
    @Expose
    private String pressureMb;
    @SerializedName("windchill_f")
    @Expose
    private String windchillF;
    @SerializedName("dewpoint_f")
    @Expose
    private int dewpointF;
    @SerializedName("wind_mph")
    @Expose
    private double windMph;

    /**
     * 
     * @return
     *     The windGustMph
     */
    public int getWindGustMph() {
        return windGustMph;
    }

    /**
     * 
     * @param windGustMph
     *     The wind_gust_mph
     */
    public void setWindGustMph(int windGustMph) {
        this.windGustMph = windGustMph;
    }

    /**
     * 
     * @return
     *     The tempF
     */
    public double getTempF() {
        return tempF;
    }

    /**
     * 
     * @param tempF
     *     The temp_f
     */
    public void setTempF(double tempF) {
        this.tempF = tempF;
    }

    /**
     * 
     * @return
     *     The observationLocation
     */
    public ObservationLocation getObservationLocation() {
        return observationLocation;
    }

    /**
     * 
     * @param observationLocation
     *     The observation_location
     */
    public void setObservationLocation(ObservationLocation observationLocation) {
        this.observationLocation = observationLocation;
    }

    /**
     * 
     * @return
     *     The tempC
     */
    public double getTempC() {
        return tempC;
    }

    /**
     * 
     * @param tempC
     *     The temp_c
     */
    public void setTempC(double tempC) {
        this.tempC = tempC;
    }

    /**
     * 
     * @return
     *     The relativeHumidity
     */
    public String getRelativeHumidity() {
        return relativeHumidity;
    }

    /**
     * 
     * @param relativeHumidity
     *     The relative_humidity
     */
    public void setRelativeHumidity(String relativeHumidity) {
        this.relativeHumidity = relativeHumidity;
    }

    /**
     * 
     * @return
     *     The weather
     */
    public String getWeather() {
        return weather;
    }

    /**
     * 
     * @param weather
     *     The weather
     */
    public void setWeather(String weather) {
        this.weather = weather;
    }

    /**
     * 
     * @return
     *     The dewpointC
     */
    public int getDewpointC() {
        return dewpointC;
    }

    /**
     * 
     * @param dewpointC
     *     The dewpoint_c
     */
    public void setDewpointC(int dewpointC) {
        this.dewpointC = dewpointC;
    }

    /**
     * 
     * @return
     *     The windchillC
     */
    public String getWindchillC() {
        return windchillC;
    }

    /**
     * 
     * @param windchillC
     *     The windchill_c
     */
    public void setWindchillC(String windchillC) {
        this.windchillC = windchillC;
    }

    /**
     * 
     * @return
     *     The pressureMb
     */
    public String getPressureMb() {
        return pressureMb;
    }

    /**
     * 
     * @param pressureMb
     *     The pressure_mb
     */
    public void setPressureMb(String pressureMb) {
        this.pressureMb = pressureMb;
    }

    /**
     * 
     * @return
     *     The windchillF
     */
    public String getWindchillF() {
        return windchillF;
    }

    /**
     * 
     * @param windchillF
     *     The windchill_f
     */
    public void setWindchillF(String windchillF) {
        this.windchillF = windchillF;
    }

    /**
     * 
     * @return
     *     The dewpointF
     */
    public int getDewpointF() {
        return dewpointF;
    }

    /**
     * 
     * @param dewpointF
     *     The dewpoint_f
     */
    public void setDewpointF(int dewpointF) {
        this.dewpointF = dewpointF;
    }

    /**
     * 
     * @return
     *     The windMph
     */
    public double getWindMph() {
        return windMph;
    }

    /**
     * 
     * @param windMph
     *     The wind_mph
     */
    public void setWindMph(double windMph) {
        this.windMph = windMph;
    }

}
