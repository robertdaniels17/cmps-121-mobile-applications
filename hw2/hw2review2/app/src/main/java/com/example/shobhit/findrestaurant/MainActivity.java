package com.example.shobhit.findrestaurant;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shobhit.findrestaurant.response.TopLevelResponse;


import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.GET;

public class MainActivity extends AppCompatActivity {

    TextView temperatureTextView, cityTextView, elevationTextView, weatherTextView,
            humidityTextView, windSpeedTextView, windGustSpeedTextView;
    Button getWeatherButton;
    Toast toast;
    ProgressBar spinner;

	public static String LOG_TAG = "MyApplication";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

        temperatureTextView = (TextView) findViewById(R.id.temperatureTextView);
        temperatureTextView.setText("");
        cityTextView = (TextView) findViewById(R.id.cityTextView);
        elevationTextView = (TextView) findViewById(R.id.elevationTextView);
        humidityTextView = (TextView) findViewById(R.id.humidityTextView);
        weatherTextView = (TextView) findViewById(R.id.weatherTextView);
        windSpeedTextView = (TextView) findViewById(R.id.windSpeedTextView);
        windGustSpeedTextView = (TextView) findViewById(R.id.windGustSpeedTextView);

        getWeatherButton = (Button) findViewById(R.id.getWeatherButton);

        toast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER_VERTICAL,0,0);
        toast.setGravity(Gravity.CENTER_HORIZONTAL,0,0);

        spinner=(ProgressBar)findViewById(R.id.progressBar);
        spinner.setVisibility(View.GONE);
    }

    public void fetchWeather(View v) {
        getWeatherButton.setClickable(false);
        spinner.setVisibility(View.VISIBLE);

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://luca-teaching.appspot.com/weather/")
                .addConverterFactory(GsonConverterFactory.create())	//parse Gson string
                .client(httpClient)	//add logging
                .build();

        WeatherService service = retrofit.create(WeatherService.class);
        Call<TopLevelResponse> queryResponseCall = service.getWeather();

        queryResponseCall.enqueue(new Callback<TopLevelResponse>() {
            @Override
            public void onResponse(Response<TopLevelResponse> response) {
                getWeatherButton.setClickable(true);
                spinner.setVisibility(View.GONE);

                if (response.code() == 200) {
                    if (response.body().response.result.equals("ok")) {
                        Log.i(LOG_TAG, "result: ok");
                        temperatureTextView.setText(String.valueOf(response.body().response.conditions.tempF) + "°");
                        cityTextView.setText(response.body().response.conditions.observationLocation.city);
                        elevationTextView.setText(response.body().response.conditions.observationLocation.elevation);
                        humidityTextView.setText(response.body().response.conditions.relativeHumidity);
                        weatherTextView.setText(response.body().response.conditions.weather);
                        windSpeedTextView.setText(String.valueOf(response.body().response.conditions.windMph));
                        windGustSpeedTextView.setText(String.valueOf(response.body().response.conditions.windGustMph));
                        toast.cancel();
                    }else{
                        Log.i(LOG_TAG, "result: error");
                        setToEmptyStrings();
                        toast.setText("There was an error(200), try again");
                        toast.show();
                    }

                }else if (response.code() == 500) {
                    Log.i(LOG_TAG, "Code is: " + response.code());
                    setToEmptyStrings();
                    toast.setText("There was a server error(500), try again");
                    toast.show();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                getWeatherButton.setClickable(true);
                spinner.setVisibility(View.GONE);
            }
        });
    }

    public void setToEmptyStrings() {
        temperatureTextView.setText("");
        cityTextView.setText("");
        elevationTextView.setText("");
        humidityTextView.setText("");
        weatherTextView.setText("");
        windSpeedTextView.setText("");
        windGustSpeedTextView.setText("");
    }

	@Override
	public void onResume(){
        super.onResume();
	}

    public interface WeatherService {
        @GET("default/get_weather/")
        Call<TopLevelResponse> getWeather();
    }

}
