package com.example.shobhit.findrestaurant.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopLevelResponse {
    @SerializedName("response")
    @Expose
    public HTTPResponse response;

}
