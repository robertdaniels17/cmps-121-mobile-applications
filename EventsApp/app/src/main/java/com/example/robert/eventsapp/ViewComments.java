package com.example.robert.eventsapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

//import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;

/**
 * Created by asantelli on 3/13/16.
 */


public class ViewComments extends AppCompatActivity {

    String objectId;
    Bundle bundle;

    ListView myListView;
    SimpleAdapter adapter;
    List<List<Map<String, String>>> outerList = new ArrayList<List<Map<String, String>>>();
    List<Map<String, String>> list = new ArrayList<Map<String, String>>();
    ParseObject event;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_view_comments);

        bundle = getIntent().getExtras();
        objectId = bundle.getString("objectId");

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");
        query.whereEqualTo("objectId", objectId);

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> eventList, ParseException e) {
                if (e == null && eventList.size() > 0) {
                    Map<String, String> map = null;

                    JSONObject jobject;
                    Iterator<?> keys;

                    Log.i("DEBUG", "bug");
                    Object object = eventList.get(0);
                    JSONArray comments = ((ParseObject) object).getJSONArray("Comments");

                    for (int j = 0; j < comments.length(); j++) {
                        map = new HashMap<String, String>();
                        try {
                            jobject = (JSONObject) comments.get(j);
                            keys = jobject.keys();

                            while( keys.hasNext() ){
                                String key = (String)keys.next();
                                String value = jobject.getString(key);
                                map.put(key, value);

                            }
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }

                        list.add(map);
                    }


                    myListView = (ListView) findViewById(R.id.listView);
                    adapter = new SimpleAdapter(ViewComments.this, list,
                            R.layout.comment_row, new String[]{"comment", "user"},
                            new int[]{R.id.comment_text, R.id.user}
                    );
                    myListView.setAdapter(adapter);

                } else {
                    Log.i("DEBUG", "No Events on day");
                    Toast.makeText(ViewComments.this, "No comments yet", Toast.LENGTH_SHORT).show();
                }
                try {
                    Thread.sleep(500);                 //1000 milliseconds is one second.
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }

            }
        });

        myListView = (ListView) findViewById(R.id.listView);
        adapter = new SimpleAdapter(ViewComments.this, list,
                R.layout.comment_row, new String[]{"comment", "user"},
                new int[]{R.id.comment_text, R.id.user}
        );
        myListView.setAdapter(adapter);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        //client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

    }

    public void submit(View v) {
        EditText x = (EditText) findViewById(R.id.editText);
        Map<String, String> map = new HashMap<String, String>();

        Log.i("DEBUG", x.getText().toString());

        List<Map<String, String>> list2 = new ArrayList<Map<String, String>>();

        map.put("comment", x.getText().toString());
        map.put("user", App.getAcct());
        list.add(map);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");

        query.getInBackground(objectId, new GetCallback<ParseObject>() {
            public void done(ParseObject event, ParseException e) {
                event.put("Comments", list);
                event.saveInBackground();
            }
        });

        adapter.notifyDataSetChanged();




    }
}