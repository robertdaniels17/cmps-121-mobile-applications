package com.example.robert.eventsapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Robert on 3/11/16.
 */



public class FoundEvents extends AppCompatActivity {

        private ArrayAdapter<String> arrayAdapter;
        ProgressDialog progressDialog, progress, googleProg;
        Bundle extras;
        TextView creator;
        final ArrayList<String> arrayList = new ArrayList<String>();
        ListView myListView;

        TextView RedUser;

        List<Map<String, String>> list = new ArrayList<Map<String, String>>();

        String creatr = "";

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            //
               extras = getIntent().getExtras();

            //
            super.onCreate(savedInstanceState);

            progress = new ProgressDialog(this);


            googleProg = new ProgressDialog(this);
            googleProg.setMessage("Calling Google Calendar API ...");

              setContentView(R.layout.activity_found_events);

            SharedPreferences prefs = this.getSharedPreferences(
                    "com.example.robert", Context.MODE_PRIVATE);

            prefs.edit().putBoolean("firstCreateEvent", true).apply();

            //display criteria on bottom
            TextView temp = (TextView) findViewById(R.id.textView);
            temp.setText("  Events for " +
                   extras.getString("eventtype") + " at " + extras.getString("location"));

            myListView = (ListView) findViewById(R.id.listView2);
            //set the listener for list elements and sends the data
            myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    //Toast.makeText(FoundEvents.this, idList.get(position), Toast.LENGTH_SHORT).show();
                    //Toast.makeText(FoundEvents.this, list.get(position).get("id"), Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(FoundEvents.this, ViewEvent.class);

                    intent.putExtra("objectId", list.get(position).get("id"));

                    startActivity(intent);
                }
            });
        }

        public void setFields(){

            creator.setText(App.getAcct());
        }
        @Override
        public void onBackPressed() {
            super.onBackPressed();
        }

        @Override
        public void onResume(){
            super.onResume();

            //clear the list so events don't accumulate
            parseRefresh(findViewById(android.R.id.content));
        }

        public void createEvent(View v){

            Intent intent = new Intent(FoundEvents.this, createEvent.class);

            intent.putExtras(extras);

            startActivity(intent);
        }

        public void parseRefresh(View v){
            //clear the list so events don't accumulate
            list.clear();
            myListView = (ListView) findViewById(R.id.listView2);
            SimpleAdapter adapter = new SimpleAdapter(FoundEvents.this, list,
                    R.layout.event_row, new String[] {"eventName", "id", "username", "time", "peopleCount"},
                    new int[] {R.id.eventName, R.id.userid, R.id.username, R.id.time, R.id.peopleCount}
            );
            myListView.setAdapter(adapter);
            fetchParse();
        }

        public void fetchParse(){

            ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");

            if (extras.getString("eventtype").equals("All")){

            }

            else{
                query.whereEqualTo("eventtype", extras.getString("eventtype"));
            }

            if (extras.getString("location").equals("All")){

            }
            else{
                query.whereEqualTo("location", extras.getString("location"));

            }

            Log.i("DEBUG", extras.getString("location"));

            query.orderByAscending("year");
            query.addAscendingOrder("month");
            query.addAscendingOrder("day");
            query.addAscendingOrder("am_pm");
            query.addAscendingOrder("hour");
            query.addAscendingOrder("minute");


            progressDialog = ProgressDialog.show(FoundEvents.this, "",
                    "Fetching available events...", true);

            //final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>{};
            //final ArrayList<String> arrayList = new ArrayList<String>();

            query.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> eventList, ParseException e) {
                    if (e == null && eventList.size() > 0) {
                        Log.i("DEBUG", "Retrieved " + eventList.size() + " event. Event name: " +
                                eventList.get(0).getString("eventName"));
                        CharSequence c = "Retrieved " + eventList.size() + " event(s).";
                        Toast.makeText(FoundEvents.this, c, Toast.LENGTH_SHORT).show();
                        Map<String, String> map;
                        ArrayList<String> userEventNames = new ArrayList<String>();

                        for (int i = 0; i < eventList.size(); i++) {
                            map = new HashMap<String, String>();
                            Object object = eventList.get(i);

                            int hour = ((ParseObject) object).getInt("hour");
                            String hourString = (hour < 1) ? "12" : hour + "";
                            int minute = ((ParseObject) object).getInt("minute");
                            String minuteString = (minute < 10) ? "0" + minute : "" + minute;
                            String people = ((ParseObject) object).getInt("numJoined") + "/" +
                                    ((ParseObject) object).getString("numPeople");

                            map.put("eventName", ((ParseObject) object).getString("eventName"));
                            map.put("id", ((ParseObject) object).getObjectId());
                            map.put("username", ((ParseObject) object).getString("userName"));
                            map.put("time", (hourString + ":" + minuteString + " " +
                                    ((ParseObject) object).getString("AM_PM")) + " on " + ( ((ParseObject) object).getInt("month") + "/" +
                                    ((ParseObject) object).getInt("day") + "/" +
                                    ((ParseObject) object).getInt("year") ));
                            map.put("peopleCount", people + " people");
                            list.add(map);

                            JSONArray usersAttend = ((ParseObject) object).getJSONArray("usersAttending");
                            for (int j = 0; j < usersAttend.length(); j++) {
                                try {
                                    if (App.getAcct().equalsIgnoreCase(
                                            usersAttend.getJSONObject(j).toString()))
                                        userEventNames.add(((ParseObject) object).getString("eventName"));
                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }
                            }

                        }

                        SimpleAdapter adapter = new SimpleAdapter(FoundEvents.this, list,
                                R.layout.event_row, new String[]{"eventName", "id", "username", "time", "peopleCount"},
                                new int[]{R.id.eventName, R.id.userid, R.id.username, R.id.time, R.id.peopleCount}
                        );

                        myListView.setAdapter(adapter);

                        extras.putStringArrayList("userEventNames", userEventNames);


                    } else {
                        Log.i("DEBUG", "No events on this day");
                        Toast.makeText(FoundEvents.this, "No events found", Toast.LENGTH_SHORT).show();
                    }
                    try {
                        Thread.sleep(500);                 //1000 milliseconds is one second.
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                    progressDialog.dismiss();
                }
            });


        }




        public void viewMyEvents(View v){
            ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");
            String[] user= {App.getAcct()};
            query.whereContainedIn("usersAttending", Arrays.asList(user));
            query.orderByAscending("year");
            query.addAscendingOrder("month");
            query.addAscendingOrder("day");
            query.addAscendingOrder("AM_PM");
            query.addAscendingOrder("hour");
            query.addAscendingOrder("minute");

            query.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> eventList, ParseException e) {
                    if (e == null && eventList.size() > 0) {
                        String s = "";
                        for (int i = 0; i < eventList.size(); i++) {
                            Object object = eventList.get(i);

                            int hour = ((ParseObject) object).getInt("hour");
                            String hourString = (hour < 1) ? "12" : hour+"";
                            int minute = ((ParseObject) object).getInt("minute");
                            String minuteString = (minute < 10) ? "0"+minute : ""+minute;

                            String eventName = ((ParseObject) object).getString("eventName");
                            String userName = ((ParseObject) object).getString("userName");
                            String time = ((hourString + ":" + minuteString + " " +
                                    ((ParseObject) object).getString("AM_PM")));
                            String eventtype = ((ParseObject) object).getString("eventtype");
                            String loc = ((ParseObject) object).getString("location");
                            String day = ( ((ParseObject) object).getInt("month") + "/" +
                                    ((ParseObject) object).getInt("day") + "/" +
                                    ((ParseObject) object).getInt("year") );


                            s += eventName + "\nCreator: " + userName + "\n" +
                                    "eventtype: " + eventtype + "\nLocation: " + loc + "\n" +
                                    day + " at " + time + "\n\n" ;
                        }

                        new AlertDialog.Builder(FoundEvents.this)
                                .setTitle("Your events:")
                                .setMessage(s)
                                .setNegativeButton(android.R.string.no, null)
                                .create().show();

                    } else {
                        Log.i("DEBUG", "No events on day");
                        Toast.makeText(FoundEvents.this, "No events found", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        public class getCalData extends AsyncTask<Void, Void, List<String>> {
            private com.google.api.services.calendar.Calendar mService = null;
            private Exception mLastError = null;
            private Map<String, String> map;
            private String loc = "";

            public getCalData(GoogleAccountCredential credential, String location) {
                HttpTransport transport = AndroidHttp.newCompatibleTransport();
                JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();

                mService = new com.google.api.services.calendar.Calendar.Builder(
                        transport, jsonFactory, credential)
                        .setApplicationName("Google Calendar API Android Quickstart")
                        .build();
                map = new HashMap<String, String>();
                loc = location;


            }

            /**
             * Background task to call Google Calendar API.
             *
             * @param params no parameters needed for this task.
             */
            @Override
            protected List<String> doInBackground(Void... params) {

                try {
                    return getDataFromApi();
                } catch (Exception e) {
                    mLastError = e;
                    cancel(true);
                    return null;
                }
            }

            /**
             * Fetch a list of the next 10 events from the primary calendar.
             *
             * @return List of Strings describing returned events.
             * @throws IOException
             */
            private List<String> getDataFromApi() throws IOException {
                // List the next 10 events from the primary calendar.
                DateTime now = new DateTime(System.currentTimeMillis());
                long oneDay = (long) 1000.0 * 60 * 60 * 24;
                DateTime upcomingWeek = new DateTime(System.currentTimeMillis()+(oneDay*7));
                List<String> eventStrings = new ArrayList<String>();
                Events events = mService.events().list(map.get(loc))
                        .setTimeMin(now)
                        .setTimeMax(upcomingWeek)
                        .setOrderBy("startTime")
                        .setSingleEvents(true)
                        .execute();
                List<Event> items = events.getItems();

                for (Event event : items) {
                    DateTime start = event.getStart().getDateTime();
                    if (start == null) {
                        // All-day events don't have start times, so just use
                        // the start date.
                        start = event.getStart().getDate();
                    }
                    DateTime end = event.getEnd().getDateTime();
                    if (end == null) {
                        // All-day events don't have start times, so just use
                        // the start date.
                        end = event.getEnd().getDate();
                    }

                    String sTemp = String.format("%s", start);
                    String[] startDate = sTemp.split("T");


                    eventStrings.add(
                            String.format("%s\n%s\n---------------------------------------", event.getSummary(), startDate[0]));
                }
                return eventStrings;
            }

            @Override
            protected void onPreExecute() {
                //mOutputText.setText("");
                //progress.show();
            }


            @Override
            protected void onCancelled() {
                //progress.hide();
            }

        }
    }
