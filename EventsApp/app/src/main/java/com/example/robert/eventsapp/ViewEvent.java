package com.example.robert.eventsapp;

import android.graphics.Color;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Arrays;

/**
 * Created by Robert on 3/7/16.
 */

public class ViewEvent extends AppCompatActivity {

    Bundle bundle;
    String objectId;
    String event,title,creatr,pref,join,des,date,time = "";
    String note = "Note: Red Users are approved by UCSC";

    TextView chosenEvent, eventTitle, creator, joined, desc, eventDay, eventTime, note1;
    Button joinButton;

    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_event);

        progress = new ProgressDialog(this);


        chosenEvent = (TextView) findViewById(R.id.chosenEvent);
        eventTitle = (TextView) findViewById(R.id.eventTitle);
        creator = (TextView) findViewById(R.id.eventCreator);
        joined = (TextView) findViewById(R.id.NumJoined);
        desc = (TextView) findViewById(R.id.desc);
        eventDay = (TextView) findViewById(R.id.chosenDay);
        eventTime = (TextView) findViewById(R.id.chosenTime);

        joinButton = (Button) findViewById(R.id.joinEvent);

        bundle = getIntent().getExtras();
        objectId = bundle.getString("objectId");
        initializeEvent();
        //setFields();

    }

    public void setFields(){

        chosenEvent.setText(event);
        eventTitle.setText(title);
        creator.setText(creatr);
        //preferred.setText(pref);
        joined.setText("People going:\n" + join + "/" + pref);
        desc.setText(des);
        eventDay.setText(date);
        eventTime.setText(time);



    }

    public void joinEvent(View v){

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");

        // Retrieve the object by id
        query.getInBackground(objectId, new GetCallback<ParseObject>() {
            public void done(ParseObject event, ParseException e) {

                boolean alreadyJoined = false;

                JSONArray usersAttending = event.getJSONArray("usersAttending");

                for(int i = 0; i < usersAttending.length(); i++) {
                    try {

                        String user = usersAttending.get(i).toString();
                        if(user.equalsIgnoreCase(App.getAcct())) alreadyJoined = true;

                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }
                }

                if (e == null) {
                    if(!alreadyJoined) {
                        event.put("numJoined", event.getNumber("numJoined").intValue() + 1);
                        event.addAllUnique("usersAttending", Arrays.asList(App.getAcct()));
                        joined.setText("People joined:\n" + (event.getNumber("numJoined").intValue()) + "/" + pref);
                        joinButton.setText("Un-join");
                        Toast.makeText(ViewEvent.this, "Joined event", Toast.LENGTH_SHORT).show();
                    }

                    else{
                        event.put("numJoined", event.getNumber("numJoined").intValue() - 1);
                        event.removeAll("usersAttending", Arrays.asList(App.getAcct()));
                        joined.setText("People joined:\n" + (event.getNumber("numJoined").intValue()) + "/" + pref);
                        joinButton.setText("Join");
                        Toast.makeText(ViewEvent.this, "Unjoined event", Toast.LENGTH_SHORT).show();
                    }
                    event.saveInBackground();
                }
            }
        });
    }

    public void initializeEvent() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");

        progress = progress.show(ViewEvent.this, "",
                "Fetching event data...", true);

        query.getInBackground(objectId, new GetCallback<ParseObject>() {
            public void done(ParseObject event, ParseException e) {

                boolean alreadyJoined = false;

                JSONArray usersAttending = event.getJSONArray("usersAttending");

                for(int i = 0; i < usersAttending.length(); i++) {
                    try {

                        String user = usersAttending.get(i).toString();
                        if(user.equalsIgnoreCase(App.getAcct())) alreadyJoined = true;

                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }
                }

                if(alreadyJoined){
                    joinButton.setText("Un-join");
                }
                else{
                    joinButton.setText("Join");
                }

                if (e == null){
                    //users in this if statement need to be approved first
                    title = event.getString("eventName");
                    if (event.getString("userName").equals("rwdaniel@ucsc.edu")){
                        creator.setTextColor(Color.RED);
                        creatr = "By: " + event.getString("userName");
                    }
                    else{
                        creator.setTextColor(Color.BLACK);
                        creatr = "By: " + event.getString("userName");
                    }

                    //creatr.setTextColor(Color.parseColor("#ffffff"));
                    pref = event.getString("numPeople");
                    join = event.getInt("numJoined")+"";
                    des = "Description: \n" + event.getString("description");
                    date = "Date: " + event.getInt("month") + "/" + event.getInt("day")
                            + "/" + event.getInt("year");
                    time = "Time: " +
                            ((event.getInt("hour") > 1) ? event.getInt("hour") : 12) + ":" +
                            ((event.getInt("minute") > 10) ? event.getInt("minute") : "0"+(event.getInt("minute"))) +
                            " " + event.getString("AM_PM");


                    setFields();
                } else{

                }

                try {
                    Thread.sleep(500);                 //1000 milliseconds is one second.
                } catch(InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }

                progress.dismiss();

            }

        });

    }
    public void comments(View view) {
        Intent intent = new Intent(ViewEvent.this, ViewComments.class);

        intent.putExtra("objectId", objectId);

        startActivity(intent);

    }
}
