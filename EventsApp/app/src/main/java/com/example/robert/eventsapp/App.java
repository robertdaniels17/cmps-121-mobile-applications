package com.example.robert.eventsapp;


import android.app.Application;

import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.parse.Parse;

/**
 * Created by Robert on 3/6/16.
 */


public class App extends Application{

    static private String account = "";
    static private GoogleAccountCredential cred = null;

    @Override public void onCreate() {
        super.onCreate();

        Parse.enableLocalDatastore(getApplicationContext());

        Parse.initialize(this, "xH2I8viOR3PpAhIynAUW36nE3firmTDHYUxsZVm8", "SAl9alUrQ6VDcVZ0CJY001Mfa0FDruOceU1A3trW");

    }

    static public void setCred(GoogleAccountCredential c){
        cred = c;
    }

    static public void setAcct() {
        account = cred.getSelectedAccountName();
    }

    static public String getAcct(){
        return account;
    }

    static public GoogleAccountCredential getCred(){
        return cred;
    }
}
