package com.example.robert.eventsapp;



import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseObject;

import org.json.JSONArray;

import java.util.Calendar;

/**
 * Created by Robert on 3/7/16.
 */

public class createEvent extends FragmentActivity
        implements TimePickerFragment.OnTimeSelectedListener, DatePickerFragment.OnDateSelectedListener{

    Bundle bundle;
    ParseObject event;
    int hour;
    int minute;
    int year;
    int month;
    int day;
    String AM_PM;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event);

        bundle = getIntent().getExtras();

        event = new ParseObject("Event");

        TextView criteria = (TextView) findViewById(R.id.textView4);
        criteria.setText(bundle.getString("eventtype") + " at " + bundle.getString("location"))
        //        + bundle.getInt("month") + "/" +bundle.getInt("day") + "/" +
        //        bundle.getInt("year"))
        ;


        EditText editText = (EditText) findViewById(R.id.editText10);
        EditText editText1 = (EditText) findViewById(R.id.editText11);
        editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new TimePickerFragment();
                newFragment.show(getFragmentManager(), "timePicker");
            }


        });

        editText1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment1 = new DatePickerFragment();
                newFragment1.show(getFragmentManager(), "datePicker");
            }


        });

        //prevent keyboard
        editText.setRawInputType(InputType.TYPE_NULL);
     //   editText.setFocusable(true);Focusable(true);
        editText1.setRawInputType(InputType.TYPE_NULL);
     //   editText1.setFocusable(true);Focusable(true);
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }

    public void buttonPressed(View v){
        EditText editText = (EditText) findViewById(R.id.editText);
        String eventName = editText.getText().toString();

        editText = (EditText) findViewById(R.id.editText2);
        String preferredPeople = editText.getText().toString();

        editText = (EditText) findViewById(R.id.editText3);
        String eventDescription = editText.getText().toString();

        //editText = (EditText) findViewById(R.id.editText4);
        String userName = App.getAcct();

        //map.put("user", App.getAcct());

        if(eventName.length() > 0 && preferredPeople.length() > 0 && eventDescription.length() > 0 &&
                userName.length() > 0 && hour > 0 && year > 1) {

            if (eventName.length() < 1) {
                eventName = "No Name";
            }

            if (preferredPeople.length() < 1) {
                preferredPeople = "2";
            }

     //       if (year < 1) {

       //     }

            if (eventDescription.length() < 1) {
                eventDescription = "No Description";
            }

            if (userName.length() < 1) {
                userName = "user";
            }

            if (AM_PM.length() < 1) {
                //Toast.makeText(getApplicationContext(), "Please enter a time", Toast.LENGTH_LONG).show();
                AM_PM = "AM";
                //return;
            }

            JSONArray comment = new JSONArray();

            event.put("userName", userName);
            event.put("eventName", eventName);
            event.put("numPeople", preferredPeople); //Not the # of people joined, the # preferred, must change to int sometime
            event.put("numJoined", 1);
            event.put("description", eventDescription);
            event.put("year", year);
            event.put("month", month);
            event.put("day", day);
            event.put("minute", minute);
            event.put("hour", hour);
            event.put("AM_PM", AM_PM);
            event.put("Comments",comment);

            //int day = bundle.getInt("day");
            //int month = bundle.getInt("month");
            //int year = bundle.getInt("year");

            //event.put("day", day);
            //event.put("month", month);
            //event.put("year", year);

            String location = bundle.getString("location");
            String eventtype = bundle.getString("eventtype");

            if (eventtype.equals("All")){
                event.put("eventtype", "Other");

            }
            else{
                event.put("eventtype", eventtype);

            }

            if (location.equals("All")){
                event.put("location", "Other");

            }
            else{
                event.put("location", location);

            }


            JSONArray usersAttending = new JSONArray();
            usersAttending.put(App.getAcct());
            event.put("usersAttending", usersAttending);

            //Toast.makeText(getApplicationContext(), day + "/" + month + "/" + year, Toast.LENGTH_LONG).show();
            Log.i("DEBUG", "Saving in BG");
            //event.saveInBackground();

            event.saveInBackground();
            Log.i("DEBUG", "Saved in BG");

            try {
                Thread.sleep(1000);                 //1000 milliseconds is one second.
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }

            super.onBackPressed();

        } else if (userName.length() < 1){
            Toast.makeText(createEvent.this, "Please sign in", Toast.LENGTH_SHORT).show();

        }

        else{
            Toast.makeText(createEvent.this, "Please enter all fields", Toast.LENGTH_SHORT).show();
        }
    }

    public void onTimeSelected(int hourOfEvent, int minuteOfEvent) {
        minute = minuteOfEvent;
        EditText editText = (EditText) findViewById(R.id.editText10);

        if (hourOfEvent >= 12) {
            if (hourOfEvent == 12) {
                hour = hourOfEvent;
                AM_PM = "PM";
                if (minute > 10)
                    editText.setText(hour + ":" + minute + " " + AM_PM);
                else
                    editText.setText(hour + ":0" + minute + " " + AM_PM);
            }
            else{
                hour = hourOfEvent % 12;
                AM_PM = "PM";
                if (minute > 10)
                    editText.setText(hour + ":" + minute + " " + AM_PM);
                else
                    editText.setText(hour + ":0" + minute + " " + AM_PM);

            }

        } else {

            AM_PM = "AM";
            if (hourOfEvent == 0) {
                hour = hourOfEvent + 12;
                if (minute > 10)
                    editText.setText(hour + ":" + minute + " " + AM_PM);
                else
                    editText.setText(hour + ":0" + minute + " " + AM_PM);
            }
            else{
                hour = hourOfEvent;
                if (minute > 10)
                    editText.setText(hour + ":" + minute + " " + AM_PM);
                else
                    editText.setText(hour + ":0" + minute + " " + AM_PM);
            }
        }

    }

    public void onDateSelected(int yearOfEvent, int monthOfEvent, int dayOfEvent){

        EditText editText= (EditText) findViewById(R.id.editText11);
//        if(day > 10)
//            editText.setText(hour + ":" + minute + " " + AM_PM);
//        else
            monthOfEvent += 1;
            year = yearOfEvent;
            month = monthOfEvent;
            day = dayOfEvent;
        if (monthOfEvent < 9) {
            if (dayOfEvent < 10){
                editText.setText("0" + month + "/0" + day + "/" + year);
            }
            else {
                editText.setText("0" + month + "/" + day + "/" + year);
            }
        }
        else{
            if (dayOfEvent < 10){
                editText.setText(month + "/0" + day + "/" + year);
            }
            else {
                editText.setText(month + "/" + day + "/" + year);
            }
        }

    }
}
