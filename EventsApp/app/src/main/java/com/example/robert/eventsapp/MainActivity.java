package com.example.robert.eventsapp;

import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseObject;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity {

    private ArrayList<String> eList, locList;
    private String currentList;
    private ListView myListView;
    private ArrayAdapter<String> arrayAdapter;
    private String LOG_TAG = "DEBUG: ";
    private String chosenE = "";
    private String chosenLoc = "";
    private String chosenCriteria = "";

    public String getE(){
        return chosenE;
    }

    public String getLoc(){
        return chosenLoc;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        eList = new ArrayList<String>();
        locList = new ArrayList<String>();
        populateEvents(eList);
        populateLocs(locList);
        arrayAdapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_1, eList
        );
        currentList = "EventType";
        myListView = (ListView) findViewById(R.id.listView);
        //Make each list element clickable
        populateListView(eList, "EventType");
        myListView.setAdapter(arrayAdapter);


    }

    @Override
    public void onBackPressed() {
        switch (currentList){
            case "EventType":
                System.exit(0);
                new AlertDialog.Builder(this)
                        //finish();
                        .setTitle("Really Exit?")
                        .setMessage("Are you sure you want to exit?")
                        .setNegativeButton(android.R.string.no, null)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {

                                System.exit(0);
                            }
                        }).create().show();
                super.onBackPressed();
                break;
            case "Locations":
                //currentList = "EventType";
                chosenCriteria = "";
                updateChosenView();
                populateListView(eList, "EventType");
                break;
            default:
                break;
        }
        updateChosenView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //populate list with the given criteria
    //also adds a listener to link to the next set of criteria (if available)
    private void populateListView(ArrayList<String> list, String nextList){
        final ArrayList<String> list2 = list;
        currentList = nextList;
        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list2);
        changeTitle();
        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast toast;
                switch (currentList) {
                    case "EventType":
                        chosenE = list2.get(position);
                        currentList = "Locations";
                        chosenCriteria = list2.get(position);
                        populateListView(locList, "Locations");
                        toast = Toast.makeText(MainActivity.this, list2.get(position), Toast.LENGTH_SHORT);
                        //toast.show();
                        break;
                    case "Locations":
                        chosenLoc = list2.get(position);
                        //currentList = "Days";
                        chosenCriteria = chosenE + " : " + list2.get(position);
                        //populateListView(dayList, "Days");
                        toast = Toast.makeText(MainActivity.this, list2.get(position), Toast.LENGTH_SHORT);
                        //toast.show();

                        Intent intent = new Intent(MainActivity.this, FoundEvents.class);


                        intent.putExtra("eventtype", chosenE);
                        intent.putExtra("location", chosenLoc);

                        startActivity(intent);

                    default:
                        break;
                }
                updateChosenView();
                Log.i(LOG_TAG, currentList + " : ");
            }
        });
        myListView.setAdapter(arrayAdapter);
    }

    private void changeTitle(){
        TextView temp = (TextView) findViewById(R.id.titleView);
        temp.setText(currentList + " List");
    }

    private void updateChosenView(){
        TextView temp = (TextView) findViewById(R.id.textView2);
        temp.setText(chosenCriteria);
    }

    //Populate eList with the available Event Types
    private void populateEvents(ArrayList<String> eList){
        eList.add("All");
        eList.add("Carpool");
        eList.add("Clubs");
        eList.add("Dances/School Events");
        eList.add("Dating");
        eList.add("Food");
        eList.add("For Sale");
        eList.add("Sports");
        eList.add("Studying");
        eList.add("Other");
    }

    //Populate locList with the available locations
    private void populateLocs(ArrayList<String> locList){
        locList.add("All");
        locList.add("College Eight");
        locList.add("College Nine");
        locList.add("College Ten");
        locList.add("Cowell College");
        locList.add("Crown College");
        locList.add("Kresge College");
        locList.add("Merrill College");
        locList.add("Oaks College");
        locList.add("Porter College");
        locList.add("Stevenson College");
        locList.add("East Field");
        locList.add("East Field House Dance Studio");
        locList.add("East Remote Field (Lower Field");
        locList.add("Family Student Housing");
        locList.add("Jack Baskin Lounge");
        locList.add("Quarry/Book Store");
        locList.add("McHenry Library");
        locList.add("OPERS");
        locList.add("Redwood Grove");
        locList.add("S&E Library");
        locList.add("The Village");
        locList.add("Wellness Center (Gym)");
        locList.add("West Gym (College 8)");
        locList.add("West Tennis Courts");
        locList.add("Other");

    }

    public void popupMap(View v){
        Intent intent = new Intent(MainActivity.this, MapActivity.class);
        startActivity(intent);
    }
}
