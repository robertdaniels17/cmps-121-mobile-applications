package com.example.shobhit.findrestaurant.response;

// import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@Generated("org.jsonschema2pojo")
public class Example2 {

    @SerializedName("result")
    @Expose
    private String result;

    /**
     *
     * @return
     * The result
     */
    public String getResult() {
        return result;
    }

    /**
     *
     * @param result
     * The result
     */
    public void setResult(String result) {
        this.result = result;
    }

    public Example2 withResult(String result) {
        this.result = result;
        return this;
    }

}