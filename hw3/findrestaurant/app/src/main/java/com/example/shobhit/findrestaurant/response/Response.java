
package com.example.shobhit.findrestaurant.response;

import java.util.ArrayList;
import java.util.List;
//import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@Generated("org.jsonschema2pojo")
public class Response {

    @SerializedName("resultList")
    @Expose
    private List<ResultList> ResultLists = new ArrayList<ResultList>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Response() {
    }

    /**
     * 
     * @param
     */
    public Response(List<ResultList> ResultLists) {
        this.ResultLists = ResultLists;
    }

    /**
     * 
     * @return
     *     The venues
     */
    public List<ResultList> getResultLists() {
        return ResultLists;
    }

    /**
     * 
     * @param
     */
    public void setVenues(List<ResultList> ResultLists) {
        this.ResultLists = ResultLists;
    }

}
