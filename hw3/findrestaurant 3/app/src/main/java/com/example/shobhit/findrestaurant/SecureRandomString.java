package com.example.shobhit.findrestaurant;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * Created by Robert on 2/18/16.
 */
public final class SecureRandomString {
    private SecureRandom random = new SecureRandom();
    public String nextString() {return new BigInteger(130, random).toString(32);}
}