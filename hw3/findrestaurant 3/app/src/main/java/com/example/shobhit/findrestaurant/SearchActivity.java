package com.example.shobhit.findrestaurant;

//import org.apache.commons.lang.RandomStringUtils;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.shobhit.findrestaurant.response.Example;
import com.example.shobhit.findrestaurant.response.Example2;
import com.example.shobhit.findrestaurant.response.ResultList;
import com.example.shobhit.findrestaurant.response.Venue;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class SearchActivity extends AppCompatActivity {

	private LocationData locationData = LocationData.getLocationData();

	//These sre required for working with Foursquare API in unauthenticated mode
	//private String CLIENT_ID = RandomStringUtils.randomAlphanumeric(17).toUpperCase();
	//private String CLIENT_SECRET = "5V1JYN5BPDVJAI0LYRYKDZMBL54A3GDFGIQHU3CNC3404X5Z";
	private String user_id;
	private String nickname;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search);

		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
		user_id = settings.getString("user_id", null);
	}


	private MyAdapter aa;

	private ArrayList<ListElement> aList;

	@Override
	protected void onResume() {
		Intent intent = getIntent();
		String nickname = intent.getStringExtra(MainActivity.Nickname);//get search word

		aList = new ArrayList<ListElement>();
		aa = new MyAdapter(this, R.layout.list_element, aList);
		ListView myListView = (ListView) findViewById(R.id.listView);
		myListView.setAdapter(aa);
		aa.notifyDataSetChanged();

		getPosts();
		//searchRestaurants(cuisine);//search restaurants serving the cuisine

		super.onResume();
	}


	private void getPosts() {

		HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
		// set your desired log level
		logging.setLevel(HttpLoggingInterceptor.Level.BODY);
		OkHttpClient httpClient = new OkHttpClient.Builder()
				.addInterceptor(logging)
				.build();

		Retrofit retrofit = new Retrofit.Builder()
				.baseUrl("https://luca-teaching.appspot.com/localmessages/default/")    //We are using Foursquare API to get data
				.addConverterFactory(GsonConverterFactory.create())    //parse Gson string
				.client(httpClient)    //add logging
				.build();

		FourSquareService service = retrofit.create(FourSquareService.class);

		//convert latitude and longitude into acceptable format
		//String ll = Double.toString(locationData.getLocation().getLatitude()) + ","
		//        + Double.toString(locationData.getLocation().getLongitude());

		//Call<Example> queryResponseCall =
		//		service.listRestaurants(CLIENT_ID, CLIENT_SECRET, "4d4b7105d754a06374d81259", 20130815, ll, cuisine, 2000);

		float lat = (float) locationData.getLocation().getLatitude();
		float lng = (float) locationData.getLocation().getLongitude();

		Call<Example> queryResponseCall =
				service.getMessages(lat, lng, user_id);

		//Call retrofit asynchronously
		queryResponseCall.enqueue(new Callback<Example>() {
			@Override
			public void onResponse(Response<Example> response) {
				List<ResultList> messages = getMessages(response.body());
				aList.clear();
				for (int i = 0; i < messages.size(); i++) {
					aList.add(new ListElement(messages.get(i).getMessage(), messages.get(i).getNickname(), messages.get(i).getUserId()));
				}

				if (messages.size() == 0)
					((TextView) findViewById(R.id.noResult)).setVisibility(View.VISIBLE);//show not found

				// We notify the ArrayList adapter that the underlying list has changed,
				// triggering a re-rendering of the list.
				aa.notifyDataSetChanged();
			}

			@Override
			public void onFailure(Throwable t) {
				// Log error here since request failed
			}

			//parse the response for list of restaurants
			private List<ResultList> getMessages(Example response) {
				List<ResultList> messages;
				//	if(response.getMeta().getCode() == 200)
				//	{
				messages = response.getResultList();
				//	}
				//	else
				//		venues = new ArrayList<Venue>();
				return messages;
			}
		});
	}

	/**
	 * Foursquare api https://developer.foursquare.com/docs/venues/search
	 */
	public interface FourSquareService {
		@GET("get_messages")
		Call<Example> getMessages(@Query("lat") Float latitude,
								  @Query("lng") Float longitude,
								  @Query("user_id") String user_id);    //catgory id dor Foof


	@GET("post_message")
	Call<Example2> postMessage(@Query("lat") Float latitude,
						   @Query("lng") Float longitude,
						   @Query("user_id") String userId,
						   @Query("nickname") String nickname,
						   @Query("message") String message,
						   @Query("message_id") String messageId);

	}
/*
	public void entermessage(View v){

		EditText editText = (EditText) findViewById(R.id.editText2);
	}
	*/

	public void post(View v){

		EditText editText = (EditText) findViewById(R.id.editText);

		HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
		// set your desired log level
		logging.setLevel(HttpLoggingInterceptor.Level.BODY);
		OkHttpClient httpClient = new OkHttpClient.Builder()
				.addInterceptor(logging)
				.build();

		Retrofit retrofit = new Retrofit.Builder()
				.baseUrl("https://luca-teaching.appspot.com/localmessages/default/")	//We are using Foursquare API to get data
				.addConverterFactory(GsonConverterFactory.create())	//parse Gson string
				.client(httpClient)	//add logging
				.build();

		FourSquareService service = retrofit.create(FourSquareService.class);


		float lat = (float)locationData.getLocation().getLatitude();
		float lng = (float)locationData.getLocation().getLongitude();

		SecureRandomString srs = new SecureRandomString();
		String message_id = srs.nextString();

		Call<Example2> queryResponseCall =
				service.postMessage(lat, lng, user_id, nickname, editText.getText().toString(), message_id);

		//Call retrofit asynchronously
		queryResponseCall.enqueue(new Callback<Example2>() {
			@Override
			public void onResponse(Response<Example2> response) {

			}

			@Override
			public void onFailure(Throwable t) {
				// Log error here since request failed
			}
		});

		TextView temp = (TextView) findViewById(R.id.noResult);
		temp.setVisibility(View.INVISIBLE);
		getPosts();
	}


	public void refresh(View v){
		TextView temp = (TextView) findViewById(R.id.noResult);
		temp.setVisibility(View.INVISIBLE);
		getPosts();
	}

}