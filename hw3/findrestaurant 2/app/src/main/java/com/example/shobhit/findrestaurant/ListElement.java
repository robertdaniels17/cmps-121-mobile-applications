package com.example.shobhit.findrestaurant;

/**
 * Created by shobhit on 1/24/16.
 */
public class ListElement {
    ListElement() {};

    ListElement(String tl, String nickname, String ul) {
        restaurantName = tl;
        name = nickname;
        user_id = ul;
    }

    public String user_id;
    public String restaurantName;
    public String name;  //Restaurant details button. Currently not implemented
}
